#if defined(ENABLE_CONTOUR_LINES)
uniform float     contourLineInterval; // A zero interval disables the line
uniform float     secondaryContourLineInterval; // A zero interval disables the line
uniform vec4      contourLineColor; // Stores both the color and opacity
#endif