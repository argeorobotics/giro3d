/**
 * Giro3D version.
 *
 * @type {string}
 */
const VERSION = 'latest';

export default VERSION;
